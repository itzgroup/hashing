/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.hashing;

import java.util.Scanner;

/**
 *
 * @author 66955
 */
public class Test {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Hashing<String, Integer> h = new Hashing<String, Integer>();
        String prodID = kb.next();
        while (!prodID.equals("*")) {
            int price = kb.nextInt();
            h.put(prodID, price);
            prodID = kb.next();
        }

        String input = kb.next();

        if (h.get(input) == null) {
            System.out.println(h.remove(input));
            System.out.println("null");
        } else {
            System.out.println(h.get(input));
        }
    }
}
