/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.hashing;

/**
 *
 * @author 66955
 */
public class Hashing<Key, Value> {

    private int m = 10;
    private Value[] vals = (Value[]) new Object[m];
    private Key[] keys = (Key[]) new Object[m];

    private int hash(Key key) {
        return (key.hashCode() & 0X7FFFFFFF) % m;
    }

    public Value get(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
        }
        return null;
    }

    public void put(Key key, Value val) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                keys[i] = key;
                vals[i] = val;
                break;
            }
            i = (i + 1) % m;
        }
        keys[i] = key;
        vals[i] = val;
    }

    public int remove(Key key) {
        int i = hash(key);
        while (keys[i] != null) {
            if (key.equals(keys[i])) {
                int temp = (int) keys[i];
                keys[i] = null;
                return temp;
            }
            i = (i + 1) % m;
        }
        return 0;
    }

}
